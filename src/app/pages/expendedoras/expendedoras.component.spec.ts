import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpendedorasComponent } from './expendedoras.component';

describe('ExpendedorasComponent', () => {
  let component: ExpendedorasComponent;
  let fixture: ComponentFixture<ExpendedorasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpendedorasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpendedorasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
