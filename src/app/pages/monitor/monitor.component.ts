import { Component, OnInit, ViewChild } from '@angular/core';
import { MdbTableDirective } from 'mdb-angular-ui-kit/table';
import { WiFiRouter } from 'src/app/interfaces/wi-fi-router';

export interface Person {
  name: string;
  position: string;
  office: string;
  age: number;
  startDate: string;
  salary: string;
}

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss'],
})
export class MonitorComponent implements OnInit {
  @ViewChild('table') table!: MdbTableDirective<WiFiRouter>;
  headers = [
    'Status',
    'Zone',
    'Users',
    'SSID',
    'Hostname',
    'Model',
    'MAC Address',
    'Uptime',
    'Tags',
    'Actions',
  ];

  dataSource: WiFiRouter[] = [
    {
      status: 'Offline',
      zone: 'Zone 1',
      users: 5,
      ssid: 'Piso Wifi',
      hostname: 'Piso Wifi',
      model: 'Model 1',
      mac: 'A3-1A-8D-04-FA-58',
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
      tags: ['Random Tag 3', 'Random Tag 4'],
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
      tags: ['Random Tag 3', 'Random Tag 4'],
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
      tags: ['Random Tag 3', 'Random Tag 4'],
    },
    {
      status: 'Online',
      zone: 'Zone 7',
      users: 2,
      ssid: 'Dollar Wifi',
      hostname: 'Dollar Wifi',
      model: 'Model 5',
      mac: 'A3-1A-8D-04-FA-58',
      uptime: '7h 12m',
      tags: ['Random Tag 3', 'Random Tag 4'],
    },
  ];

  viewMode = 'table';

  constructor() {}

  ngOnInit(): void {}

  advancedSearchRouters(value: string): void {
    this.table.search(value);
  }

  filterFn(data: any, searchTerm: string): boolean {
    // tslint:disable-next-line: prefer-const
    let [phrase, columns] = searchTerm.split(' in:').map((str) => str.trim());
    return Object.keys(data).some((key: any) => {
      if (columns?.length) {
        let result;
        columns.split(',').forEach((column) => {
          if (
            column.toLowerCase().trim() === key.toLowerCase() &&
            data[key].toLowerCase().includes(phrase.toLowerCase())
          ) {
            result = true;
          }
        });
        return result;
      }
      if (data[key] && !columns?.length) {
        return JSON.stringify(data)
          .toLowerCase()
          .includes(phrase.toLowerCase());
      }

      return;
    });
  }
}
