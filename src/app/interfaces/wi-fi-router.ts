export interface WiFiRouter {
  status: string;
  zone: string;
  users: number;
  ssid: string;
  hostname: string;
  model: string;
  mac: string;
  uptime?: string;
  tags?: string[];
}
