import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  sidenavHidden = false;
  sidenavCollapsed = true;
  title = 'Admin Portal';
}
