import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpendedorasComponent } from './pages/expendedoras/expendedoras.component';
import { HomeComponent } from './pages/home/home.component';
import { MonitorComponent } from './pages/monitor/monitor.component';
import { PlansComponent } from './pages/plans/plans.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'monitor', component: MonitorComponent },
  {
    path: 'expendedoras',
    component: ExpendedorasComponent,
  },
  { path: 'plans-and-rates', component: PlansComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
